const express = require('express'),
    router = express.Router(),
    knex = require('../db/knex');



router.post('/', function (req, res) {
    knex('comments').insert({
        text: req.body.text,
        user_id: req.body.user_id,
        post_id: req.body.post_id,
    }).then(function () {
        res.json({successful: true})        
    })
})

router.put('/', function (req, res) {
    knex('comments').where({
        id: req.body.comments_id,
        user_id: req.body.user_id,
        post_id: req.body.post_id,
    }).update({
        text: req.body.text,
    }).then(function () {
        res.json({successful: true})        
    })
})

router.delete('/', function (req, res) {
    knex('comments').where({
        id: req.body.comments_id,        
        user_id: req.body.user_id,
        post_id: req.body.post_id,
    }).delete().then(function () {
        res.json({successful: true})        
    })
})

module.exports = router;