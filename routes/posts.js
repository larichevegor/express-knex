const express = require('express'),
    router = express.Router(),
    knex = require('../db/knex');



router.get('/', function (req, res) {
    knex.from('posts')
        .innerJoin('users', 'posts.user_id', 'users.id')
        .rightJoin('comments', 'posts.id', 'comments.post_id')
        .then(function (data) {
            res.json(data)
        })
})
    
router.post('/', function (req, res) {
    knex('posts').insert({
        title: req.body.title,
        text: req.body.text,
        user_id: req.body.user_id,
    }).then(function (data) {
        res.json(data)
    })
})

router.delete('/', function (req, res) {
    knex('posts').where({
        id: req.body.id,
        user_id: req.body.user_id,
    }).delete().then(function () {
        res.json({ successful: true })
    })
})


module.exports = router;