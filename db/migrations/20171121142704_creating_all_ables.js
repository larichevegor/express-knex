
exports.up = function (knex, Promise) {
    return knex.schema.createTable('users', function (table) {
        table.increments();
        table.string('name').notNullable();
        table.string('email').notNullable();
        table.string('password').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
        .createTable('posts', function (table) {
            table.increments();
            table.string('title').notNullable();
            table.string('text').notNullable();
            table.integer('user_id').references('id').inTable('users');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
        })
        .createTable('comments', function (table) {
            table.increments();
            table.string('text').notNullable();
            table.integer('user_id').references('id').inTable('users');
            table.integer('post_id').references('id').inTable('posts');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
        })
};


exports.down = function (knex, Promise) {
   return knex.schema.dropTable('users').dropTable('posts').dropTable('comments')
};
