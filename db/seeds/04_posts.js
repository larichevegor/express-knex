
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('posts').del()
    .then(function () {
      // Inserts seed entries
      return knex('posts').insert([
        { id: 1, title: 'testtitle2131', text: 'testtext123123123', user_id: 1 },
        { id: 2, title: 'testtitle1123213', text: 'testtext1231231123123123', user_id: 2 },
      ]);
    });
};
