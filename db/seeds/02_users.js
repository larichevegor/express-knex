
exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        { id: 1, name: 'textname1', email: 'testemail1@test.com', password: 'testpass1' },
        { id: 2, name: 'textname2', email: 'testemail2@test.com', password: 'testpass2' },
      ]);
    });
};
