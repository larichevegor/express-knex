
exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('comments').del()
    .then(function () {
      // Inserts seed entries
      return knex('comments').insert([
        { id: 1, text: 'testtext123123123', user_id: 1, post_id: 1 },
        { id: 2, text: 'testtext1231231123123123', user_id: 1, post_id: 2 },
        { id: 3, text: 'testtext123123123', user_id: 1, post_id: 2 },
        { id: 4, text: 'testtext1231231123123123', user_id: 2, post_id: 1 },
      ]);
    });
};
