const express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    port = process.env.PORT || 1337,
    posts = require('./routes/posts'),
    users = require('./routes/users'),
    comments = require('./routes/comments');

const app = express();

//Body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.use('/users', users);
app.use('/posts', posts);
app.use('/comments', comments);

app.listen(port, function () {
    console.log("Server Started on PORT 1337");
});
